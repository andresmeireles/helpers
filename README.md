# README

Funções javascript uteis para front-end apenas. Toda função tem sua documentação e plugin relacionado, se necessário

### What is this repository for?

- Quick summary
- Version
- [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Setup

Cada função tem seus requisitos proprios. Nas descrições de cada função seus requisitos serão listados.

Mesmo não sendo explicitamente necessário recomendo ter instalado:

- [NodeJs](https://nodejs.org/en/)
- NPM

### calendario por dia (dayDatePicker)

Requisitos

- [flatpickr](https://flatpickr.js.org/)

##### setup do plugin

Em um tag input por a classe `day-date-picker` e um atributo `tracker`. Ao fazer isso uma será criado uma nova tag que funcionará da seguinte forma: Será criada uma nova tag que funcionará ao ser clicada, quando clicada mostrará um calendário, ao ser selecionada uma data no formato PTBR será mostrada no input e na tag original será posta a mesma data no formato Y-m-d (Ano-mês-dia):

Ex:

`<input type="text" class="day-date-picker" tracker="input1" />`

### calendario por mês (monthDatePicker)

Requisitos

- [flatpickr](https://flatpickr.js.org/)

##### setup do plugin

Em um tag input por a classe `month-date-picker` e um atributo `tracker`. Ao fazer isso uma será criado uma nova tag que funcionará da seguinte forma: Será criada uma nova tag que funcionará ao ser clicada, quando clicada mostrará um calendário, ao ser selecionada uma data no formato PTBR será mostrada no input e na tag original será posta a mesma data no formato Y-m (Ano-mês):

Ex:

`<input type="text" class="month-date-picker" tracker="input1" />`

### selecção de datas em um intervalo (rangeDatePicker)

Requisitos

- [flatpickr](https://flatpickr.js.org/)

##### setup do plugin

Em um tag input por a classe `range-date-picker` e um atributo `tracker`. Ao fazer isso uma será criado uma nova tag que funcionará da seguinte forma: Será criada uma nova tag que funcionará ao ser clicada, quando clicada mostrará um calendário, onde um intervalo de datas será selecionado, ao ser selecionada o intervalo, no formato PTBR, será mostrada no input e na tag original será posta a mesma data no formato Y-m-d (Ano-mês-dia) com a formatação Y1-m1-d1<=>Y2-m2-d2:

Ex:

`<input type="text" class="range-date-picker" tracker="input1" />`

### select melhorado com select2

Requisitos

* [jquery](https://jquery.com/)
* [select2](https://select2.org/)

Ao inserir a class `boot-select` em um `input` a biblioteca select2 irá ser ativada com a configuração preestabelecida de bootstrap.

Ex:

`<input type="text" class="boot-select" />`

### Guidelines de contruibuição

- Escrever testes
- Revisar codigo

### Com quem posso falar?

- andre2meireles@gamil.com

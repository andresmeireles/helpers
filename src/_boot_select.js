"use strict";

/**
 * required
 * jquery
 * select2
 */

document.addEventListener("DOMContentLoaded", () => {
  // activate select2
  _bootSelect();
  window.addEventListener("resize", () => _bootSelect(), true);
});

const _bootSelect = () => {
  $(function () {
    $(".boot-select").select2({
      theme: "bootstrap",
    });
  });
};

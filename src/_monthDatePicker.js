"use strict";

import flatpicker from "flatpickr";
import monthSelectPlugin from "flatpickr/dist/plugins/monthSelect";

document.addEventListener("DOMContentLoaded", () => {
    const activator = document.querySelectorAll(".month-date-picker");
    if (activator) _execute(activator);
});

/**
 *
 * @param {NodeListOf<Element>} tags
 */
const _execute = (tags) => {
    tags.forEach((tag) => {
        tag.style.display = "none";
        tag.setAttribute("type", "hidden");
        const tracker = "field" + Math.floor(Math.random(0, 1000) * 10000);
        tag.classList.add(tracker);
        tag.insertAdjacentHTML("afterend", _datefield(tracker));
    });
    document.addEventListener(
        "click",
        (element) => {
            if (element.target.classList.contains("fire-month-date-picker")) {
                const tracker = element.target.getAttribute("tracker");
                flatpicker(element.target, {
                    plugins: [
                        new monthSelectPlugin({
                            shorthand: true, //defaults to false
                            dateFormat: "m/y", //defaults to "F Y"
                            altFormat: "F Y", //defaults to "F Y"
                            // theme: "dark", // defaults to "light"
                        }),
                    ],
                    onChange: (selectedDate) => _action(selectedDate, tracker),
                    // dateFormat: "d/m/Y",
                });
            }
        },
        true
    );
};

const _datefield = (tracker) =>
    `<input type="text" class="form-control fire-month-date-picker" tracker="${tracker}" />`;

const _action = (selectedDate, tracker) => {
    const dt = selectedDate[0];
    document.querySelector(`.${tracker}`).value = `${dt.getFullYear()}-${
        dt.getMonth() + 1
    }`;
};

"use strict";

import fp from "flatpickr";

document.addEventListener("DOMContentLoaded", () => {
    const activator = document.querySelectorAll(".range-date-picker");
    if (activator) _execute(activator);
});

/**
 * @param {NodeListOf<Element>} tags
 */
const _execute = (tags) => {
    tags.forEach((tag) => _runAction(tag));
};

/**
 * @param {Element} element
 */
const _runAction = (element) => {
    _isInputTag(element);
    _tagOperations(element);

    document.addEventListener(
        "click",
        (domElement) => {
            if (
                domElement.target.classList.contains("fire-range-date-picker")
            ) {
                const tracker = domElement.target.getAttribute("tracker");
                fp(domElement.target, {
                    mode: "range",
                    minDate: "today",
                    dateFormat: "d/m/Y",
                    locale: {
                        rangeSeparator: " - ",
                    },
                    onChange: (dates) => _writeDate(dates, tracker),
                });
            }
        },
        true
    );
};

/**
 * @param {Element} tag
 */
const _isInputTag = (tag) => {
    if (tag.tagName === "INPUT") return;
    throw new Error("não é tag input").message;
};

/**
 * @param {Element} tag
 */
const _hasTracker = (tag) => {
    if (tag.getAttribute("tracker") === null)
        throw new Error("elemento não tem atributo tracker").message;
};

/**
 * @param {Element} tag
 * @returns string
 */
const _tagOperations = (tag) => {
    _hasTracker(tag);
    const dynamicClass = `${tag.getAttribute("tracker")}${Math.floor(
        Math.random() * 10000
    )}`;
    tag.setAttribute("type", "hidden");
    tag.classList.add(dynamicClass);
    tag.style.display = "none";
    _creatOperationalTag(tag, dynamicClass);

    return dynamicClass;
};

/**
 * @param {Element} tag
 * @param {string} tracker
 * @returns Element
 */
const _creatOperationalTag = (tag, tracker) => {
    let newInput = document.createElement("input");
    newInput.setAttribute("tracker", tracker);
    newInput.classList.add("form-control");
    newInput.classList.add("fire-range-date-picker");
    tag.insertAdjacentElement("afterend", newInput);
};

/**
 * @param {Array<Date>} dates
 * @param {string} tracker
 */
const _writeDate = (dates, tracker) => {
    if (dates.length < 2) return false;
    const dt1 = dates[0];
    const dt1St = `${dt1.getFullYear()}-${dt1.getMonth() + 1}-${dt1.getDate()}`;
    const dt2 = dates[1];
    const dt2St = `${dt2.getFullYear()}-${dt2.getMonth() + 1}-${dt2.getDate()}`;
    const dtSt = `${dt1St}<=>${dt2St}`;
    document.querySelector(`.${tracker}`).value = dtSt;
};

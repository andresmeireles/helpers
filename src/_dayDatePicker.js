"use strict";

import flatpicker from "flatpickr";

document.addEventListener("DOMContentLoaded", () => {
    const activator = document.querySelectorAll(".day-date-picker");
    if (activator) _execute(activator);
});

/**
 *
 * @param {NodeListOf<Element>} datepickers
 */
const _execute = (datepickers) => {
    datepickers.forEach((field) => {
        field.style.display = "none";
        const tracker = "field" + Math.floor(Math.random(0, 1000) * 10000);
        field.classList.add(tracker);
        field.insertAdjacentHTML("afterend", _datefield(tracker));
    });
    document.addEventListener(
        "click",
        (element) => {
            if (element.target.classList.contains("fire-day-date-picker")) {
                const tracker = element.target.getAttribute("tracker");
                flatpicker(element.target, {
                    onChange: (selectedDate) => _action(selectedDate, tracker),
                    dateFormat: "d/m/Y",
                });
            }
        },
        true
    );
};

const _datefield = (tracker) =>
    `<input type="text" class="form-control fire-day-date-picker" tracker="${tracker}" />`;

const _action = (selectedDate, tracker) => {
    const dt = selectedDate[0];
    document.querySelector(`.${tracker}`).value = `${dt.getFullYear()}-${
        dt.getMonth() + 1
    }-${dt.getDate()}`;
};
